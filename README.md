## Objective

A simple, single page app that helps the RTI Applicant to calculate the date to file Appeals.

## Installation Instructions

```
git clone https://gitlab.com/RTI-Hub/P1-RTI-Date-Calculator-Web/tree/documentation rti-date-calc
cd rti-date-calc
npm install
ng serve
```
See output in `http://localhost:4200/`

# Commands
 * This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.3.
 * **Development server** - Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
 * **Build** - Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
 * **Running unit tests** - Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
 * **Running end-to-end tests** - Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/). Before running the tests make sure you are serving the app via `ng serve`.
 * **Further help** - To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Target Platform

Web, Mobile, Desktop

## App Name

RTI Date Calculator

## Project Objective
 * To help RTI Applicant to calculate the dates to file Appeals, under various scenarios
 * No response / Incomplete response from PIO
 * When RTI Fee is required
 * When 3rd party information is required
 * When RTI has been transferred to more appropriate PIOs

## Internal
 * To be done as a free, open source project. Dev costs to be covered by crowdfunding.
 * To publish the RTI Date calculation logic in a separate NPM Package.

## Deliverables
 * Web App (This repo is for web app)
 * Android App (Secondary)
 * Desktop App (Secondary)

## Requirement Documents
 * [Requirement Document](https://docs.google.com/document/d/1u15JAZ-VPrR7vjGN2iSmzj3ab_6hWrePNTJ17DNacy4/edit?usp=sharing) (This file)
 * [Functional requirement](https://docs.google.com/a/grassrootsapp.in/document/d/1ssPfnnD5bgsNKOt04OZsz-Ahrq_65hsDUfjcGT46G3E/edit?usp=sharing)
 * [Web app wireframe](https://drive.google.com/a/grassrootsapp.in/file/d/0B043vrdpd62rNzF2RXZqVUo2Zkk/view?usp=sharing)
 * [Web app wireframe - Support Document](https://docs.google.com/a/grassrootsapp.in/document/d/16xBtXMEFMykpVSax6LlaFvFsC4BgIQykmwW4lkop1Kk/edit?usp=sharing)

## Content Documents
 * [Date Formulae](https://docs.google.com/spreadsheets/d/13ECLuyLlHtzgrKUXzCmTcjAUAR9acOXgukuOydB9IAk/edit#gid=258006556)
 * [English Text (JSON)](https://docs.google.com/document/d/1jD0OooD5IH3Nm4I0s7lEcAgVbyX18tEmEFGYeuxahUk/edit)
 * [Tamil Text (JSON)](https://docs.google.com/document/d/1RXd_ufErXaUwNu6HJQJAS-CQB6M2RT1MiFoJ9UKd6_w/edit)

## Test cases
 * Webapp - Test Suite

