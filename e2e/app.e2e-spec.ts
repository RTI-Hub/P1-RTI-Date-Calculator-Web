import { RtiAppPage } from './app.po';

describe('rti-app App', () => {
  let page: RtiAppPage;

  beforeEach(() => {
    page = new RtiAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
