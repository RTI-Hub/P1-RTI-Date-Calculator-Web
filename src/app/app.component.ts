import { Component			} 	from 	'@angular/core';

import { RTIDateCalcComponent} 	from 	'./rti-date-calc.component';

@Component({
    selector		            : 	'app-root',
    templateUrl					: 	'./app.component.html'
})
export class AppComponent {

    constructor() {
    }

}
