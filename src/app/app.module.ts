import { BrowserModule      }   from    '@angular/platform-browser';
import { FormsModule        }   from    '@angular/forms';
import { Http				}   from    '@angular/http';
import { HttpModule         }   from    '@angular/http';
import { NgModule           }   from    '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgbModule          }   from    '@ng-bootstrap/ng-bootstrap';
import { NgbDateParserFormatter}from 	'@ng-bootstrap/ng-bootstrap';

import { TranslateHttpLoader}   from    '@ngx-translate/http-loader';
import { TranslateLoader    }   from    '@ngx-translate/core';
import { TranslateModule    }   from    '@ngx-translate/core';

import { ShareButtonsModule }   from    'ngx-sharebuttons';

import { RtiDateFlowService }   from    './rti-date-flow.service';
import { MomentDateParser   }   from    './moment-date-parser';

import { AppComponent       }   from    './app.component';
import { HelpPopupComponent }   from    './help-popup.component';
import { InfoPopupComponent }   from    './info-popup.component';
import { SharePopupComponent}   from    './share-popup.component';
import { RTIDateCalcComponent}  from    './rti-date-calc.component';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function MomentParserFactory () {
    return new MomentDateParser("DD-MM-YYYY");
}

@NgModule({
    declarations: [
        AppComponent,
        HelpPopupComponent,
        InfoPopupComponent,
        RTIDateCalcComponent,
        SharePopupComponent
    ],

    entryComponents: [
        AppComponent,
        HelpPopupComponent,
        InfoPopupComponent,
        RTIDateCalcComponent,
        SharePopupComponent
    ],

    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,

        NgbModule.forRoot(),
        ShareButtonsModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (HttpLoaderFactory),
                deps: [Http]
            }
        })
    ],

    providers: [
        RtiDateFlowService,
        { 
            provide             :   NgbDateParserFormatter, 
            useFactory          :   (MomentParserFactory)
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
