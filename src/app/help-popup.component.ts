import { Component          } 	from 	'@angular/core';
import { ElementRef         } 	from 	'@angular/core';
import { Input              } 	from 	'@angular/core';

import { TranslateService   } 	from 	'@ngx-translate/core';
import { NgbActiveModal     } 	from 	'@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct      } 	from 	'@ng-bootstrap/ng-bootstrap';
import { NgbModal           } 	from 	'@ng-bootstrap/ng-bootstrap';

declare let ga:any;

@Component({
    selector: 'popup-modal',
    templateUrl: './help-popup.component.html',
    styleUrls: ['./help-popup.component.css']
})
export class HelpPopupComponent {

    @Input() content;

    constructor(
        public activeModal      :   NgbActiveModal,
        public translate        :   TranslateService) {
    }

    ngOnInit() {
    }

}
