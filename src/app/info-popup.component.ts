import { Component, Input   }   from    '@angular/core';
import { ViewChild          }   from    '@angular/core';
import { Http, Headers      }   from    '@angular/http';

import { NgbActiveModal     }   from    '@ng-bootstrap/ng-bootstrap';
import { NgbModal           }   from    '@ng-bootstrap/ng-bootstrap';
import { NgbModalOptions    }   from    '@ng-bootstrap/ng-bootstrap';
import { NgbTabset          }   from    "@ng-bootstrap/ng-bootstrap";
import { TranslateService   } 	from 	'@ngx-translate/core';

declare let ga:any;

@Component({
    selector                    :   'info-modal',
    templateUrl                 :   './info-popup.component.html',
    styleUrls                   :   ['./info-popup.component.css']
})
export class InfoPopupComponent {
    @Input() name;
    @Input() showFeedback       =   false;
    @ViewChild('infoTabset')
    private tabs:NgbTabset;

    public emailUser:any        =   "";
    public feedback:any         =   "";

    constructor(
        public activeModal      :   NgbActiveModal,
        private http            :   Http,
        private translate	:   TranslateService) {
    }

    ngAfterViewInit() {
        if (this.showFeedback) {
            this.tabs.select("info-contact-tab");
        }
    }

    submitFeedback() {
        var validemail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        //check if the given email address is valid or not
        if(this.emailUser.match(validemail) && this.feedback!="" && this.emailUser!=""){

            let utilityURL      =   "http://www.gethugames.in/leaders/feedback.php";
            let data            =   'email=' + this.emailUser + '&comments=' + this.feedback +'&leader=RTI-Date-Calc';

            let headers         =   new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            let utilityWS       =   this.http.post(utilityURL, data, {headers: headers});

            utilityWS.subscribe(
                utilityResponse =>  {
                    let message =   this.translate.instant("MISC.FEEDBACK_SUCCESS");
                    alert(message);
                    this.emailUser= "";
                    this.feedback=  "";
                },

                utilityErr      =>  {
                    let message =   this.translate.instant("MISC.FEEDBACK_FAILURE");
                    alert(message);
                }
            );

        } else if (!this.emailUser.match(validemail)) {
            let message         =   this.translate.instant("MISC.FEEDBACK_ER_VALID_MAIL");
            alert(message);
        } else {
            let message         =   this.translate.instant("MISC.FEEDBACK_ER_VALID_MSG");
            alert(message);
        }

    }

}
