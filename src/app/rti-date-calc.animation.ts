import { 
    animate, 
    state,  
    style, 
    trigger, 
    transition              }   from    '@angular/animations'

export function SectionEnterExitAnimation(duration: number = 0.4) {
    return trigger('enterExit', [
        transition('void => *', [
            style({ 'transform'             :   'translateX(-100%) scale(0.1)', opacity: 0}),
            animate(duration + 's ease-out')
        ]),
        transition('* => void', [
            animate(duration + 's ease-in', style({transform: 'translateX(100%) scale(0.1)', opacity: 0}))
        ])
    ])
}

export function SectionShowHideAnimation(duration: number = 0.4) {
    return trigger('showHide', [
        transition("* => 1", [
            style({ 'transform'             :   'translateY(-50%) scaleY(0.1)', opacity: 0}),
            animate(duration + 's ease-out')
        ]),
        transition("1 => *", [
            animate(duration + 's ease-in', style({transform: 'translateY(-50%) scaleY(0.1)', opacity: 0}))
        ])
    ])
}
