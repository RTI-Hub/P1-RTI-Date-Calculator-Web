import { TestBed, async     }   from    '@angular/core/testing';

import { FormsModule        }   from    '@angular/forms';
import { Http				}   from    '@angular/http';
import { HttpModule         }   from    '@angular/http';

import { NgbModule          }   from    '@ng-bootstrap/ng-bootstrap';

import { TranslateHttpLoader}   from    '@ngx-translate/http-loader';
import { TranslateLoader    }   from    '@ngx-translate/core';
import { TranslateModule    }   from    '@ngx-translate/core';

import { RtiDateFlowService }   from    './rti-date-flow.service';

import { RTIDateCalcComponent}  from    './rti-date-calc.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

describe('RTIDateCalcComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports             :   [
                FormsModule,
                HttpModule,
                NgbModule.forRoot(),
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: (HttpLoaderFactory),
                        deps: [Http]
                    }
                })
            ],
            declarations: [
                RTIDateCalcComponent
            ],

            providers: [
                RtiDateFlowService
            ],
        }).compileComponents();
    }));

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(RTIDateCalcComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it('should render title as `RTI Date Calculator`', async(() => {
        const fixture = TestBed.createComponent(RTIDateCalcComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h2').textContent).toContain('RTI Date Calculator');
    }));

    /*
    it(`should have as title 'app works!'`, async(() => {
        const fixture = TestBed.createComponent(RTIDateCalcComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('RTI Date Calculator');

    }));
    */

});
