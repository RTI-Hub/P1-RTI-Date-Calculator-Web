import { Component			} 	from 	'@angular/core';
import { ChangeDetectorRef              }       from    '@angular/core';
import { ElementRef			} 	from 	'@angular/core';
import { Input				} 	from 	'@angular/core';
import { isDevMode 			} 	from 	'@angular/core';

import { TranslateService   }   from 	'@ngx-translate/core';
import { NgbActiveModal     } 	from 	'@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct      } 	from 	'@ng-bootstrap/ng-bootstrap';
import { NgbModal           } 	from 	'@ng-bootstrap/ng-bootstrap';

import { RTIDateFlowInterface}  from 	'./rti-date-flow.interface';
import { RtiDateFlowService } 	from 	'./rti-date-flow.service';
import { SectionEnterExitAnimation} from'./rti-date-calc.animation';	
import { SectionShowHideAnimation} from'./rti-date-calc.animation';	

import { HelpPopupComponent } 	from 	'./help-popup.component';
import { InfoPopupComponent } 	from 	'./info-popup.component';
import { SharePopupComponent} 	from 	'./share-popup.component';

declare let ga:any;

@Component({
    selector                    :   'rti-date-calc',
    templateUrl                 :   './rti-date-calc.component.html',
    styleUrls                   :   ['./rti-date-calc.component.css'],
    animations                  :   [SectionEnterExitAnimation(0.4), SectionShowHideAnimation(0.4)],
    host: { //https://plnkr.co/edit/nRCpS4?p=preview
        '(document:click)': 'onClick($event)',
    },
})
export class RTIDateCalcComponent {

    public showMiniMenu         =   false;
    public selectedLanguage 	=   "en";

    private openCalendarObj;
    public options              =   [
        { value: true , display: 'MISC.YES' },
        { value: false, display: 'MISC.NO' }
    ];


    constructor(
        private _eref           :   ElementRef,
        private cdr             :   ChangeDetectorRef,
        public modalService     :   NgbModal,
        public dateService      :   RtiDateFlowService,
        public translate        :   TranslateService) {

        this.selectedLanguage   =   localStorage.getItem('prefLanguage') || 'en';
        translate.setDefaultLang('en');
        translate.use(this.selectedLanguage);

    }

    ngOnInit() {
        this.dateService.initialize();

        this.dateService.resetSubject.subscribe((state) => {
            console.log("Reset subject emitted: ", state);
            switch (state) {
                case 0:
                    this.toggleFeeRequestSection        =   true;
                    this.prevShowFeeRequestState        =   false;

                    this.toggleRTIResponseSection       =   true;
                    this.prevShowRTIResponseState       =   false;

                    this.toggleFirstAppealSection       =   true;
                    this.prevShowFirstAppealState       =   false;
                    break;
                case 1:
                    this.toggleRTIResponseSection       =   true;
                    this.prevShowRTIResponseState       =   false;

                    this.toggleFirstAppealSection       =   true;
                    this.prevShowFirstAppealState       =   false;
                    break;
                case 2:
                    this.toggleFirstAppealSection       =   true;
                    this.prevShowFirstAppealState       =   false;
                    break;

                default:
                    break;
            }
            this.cdr.detectChanges();
        });
    }

    public languageChanged() {
        console.log("Changed to ", this.selectedLanguage);
        this.translate.use(this.selectedLanguage);
        localStorage.setItem('prefLanguage', this.selectedLanguage);

        if (isDevMode()) return;

    	ga('send', 'event', {
            eventCategory       :   "language",
      	    eventLabel          :   this.selectedLanguage
    	});
    }
	
    public openInfoPopup() {
        const modalRef          =   this.modalService.open(InfoPopupComponent);

        if (isDevMode()) return;
    	ga('send', 'event', {
            eventCategory       :   "custom-event",
            eventLabel          :   "ui-action",
            eventAction         :   "info-button"
    	});

    }

    public openSharePopup():void {
        const modalRef          =   this.modalService.open(SharePopupComponent);

        if (isDevMode()) return;
    	ga('send', 'event', {
            eventCategory       :   "custom-event",
            eventLabel          :   "ui-action",
            eventAction         :   "share-button"
    	});

    }

    public openFeedbackPopup():void {
        const modalRef          =   this.modalService.open(InfoPopupComponent);
        modalRef.componentInstance.showFeedback = true;

        if (isDevMode()) return;
    	ga('send', 'event', {
            eventCategory       :   "custom-event",
            eventLabel          :   "ui-action",
            eventAction         :   "feedback-button"
    	});

    }
    
    
    public showDetailedHelp(controlName: string) {
        //debugger;
        //controlName		=   "C01";
        const modalRef          =   this.modalService.open(HelpPopupComponent);

        let title               =   this.translate.instant(controlName + ".label");
        modalRef.componentInstance.title                =   title;

        let content             =   "<h4>" + this.translate.instant(controlName + ".label") + "</h4> <br/> ";
        content                 +=   this.translate.instant(controlName + ".tooltip");
        content                 +=  " <br/> <br/> ";
        content                 +=  this.translate.instant(controlName + ".legalInfo");
        modalRef.componentInstance.content              =   content;

        if (isDevMode()) return;

    	ga('send', 'event', {
      		eventCategory		: 	"custom-event",
      		eventLabel			: 	"ui-action",
      		eventAction			: 	"control-tip-button",
      		eventValue			: 	controlName
    	});

    }

    public toggleShowMiniMenu() {
        console.log(this.showMiniMenu);
        this.showMiniMenu       =   !this.showMiniMenu;
    }

    public openDatePicker(id) {
        this.openCalendarObj    =   id;
    }

    public onClick(event) {
        if(this.openCalendarObj == undefined){
            //console.log("Dynamic id ===============");
        } else if(!this._eref.nativeElement.contains(event.target)) {
            let self = this;
            setTimeout(function(){
                self.openCalendarObj.close();    
            },10);
        }
    }

    public showHideStarted(ev) {
        console.log("Anim started", ev);
    }

    public showHideDone(ev) {
        console.log("Anim done", ev);
        if (ev.toState === "1") {
            console.log("show element");
        } else {
            console.log("hide element");
        }
    }

    public toggleBasicSection: boolean = true;
    public prevShowBasicHeaderState: boolean = false;
    public shouldShowBasicHeader(): boolean {

        let rt                  =   this.dateService.rtiFlow;
        let currentState        =   rt.rtiDate !== undefined
            && ((rt.rtiTransfer[0] !== undefined && rt.rtiTransfer[0].dateOfRTITransfer !== null)
            || rt.feePaymentNotifyDate !== undefined
            || rt.isRTIResponseReceived !== undefined);

        if (currentState !== this.prevShowBasicHeaderState && this.toggleBasicSection == true) {
            this.toggleBasicSection  =   false;
        }

        this.prevShowBasicHeaderState = currentState;

        if (this.toggleBasicSection) return true;
        return !currentState;
    }

    public toggleTransferSection: boolean[] = [true, true, true];
    public prevShowTransferState: boolean[] = [false, false, false];
    public shouldShowTransferHeader(i: number): boolean {

        let rt                  =   this.dateService.rtiFlow;
        let currentState        =   (rt.rtiTransfer[i] !== undefined 
            && rt.rtiTransfer[i].dateOfRTITransfer)
            && ((rt.rtiTransfer[i+1] !== undefined && rt.rtiTransfer[i+1].dateOfRTITransfer !== null)
            || rt.feePaymentNotifyDate !== undefined
            || rt.isRTIResponseReceived !== undefined);

        if (currentState !== this.prevShowTransferState[i] && this.toggleTransferSection[i] == true) {
            this.toggleTransferSection[i]  =   false;
        }

        this.prevShowTransferState[i] = currentState;

        if (this.toggleTransferSection[i]) return true;
        return !currentState;
    }

    public toggleFeeRequestSection: boolean = true;
    public prevShowFeeRequestState: boolean = false;
    public shouldShowFeeRequestHeader(): boolean {

        let rt                  =   this.dateService.rtiFlow;
        let currentState        =    rt.feePaymentNotifyDate !== undefined
            && rt.isRTIResponseReceived !== undefined;
 
        if (currentState !== this.prevShowFeeRequestState && this.toggleFeeRequestSection == true) {
            this.toggleFeeRequestSection  =   false;
        }

        this.prevShowFeeRequestState = currentState;

        if (this.toggleFeeRequestSection) return true;
        return !currentState;
    }

    public toggleRTIResponseSection: boolean = true;
    public prevShowRTIResponseState: boolean = false;
    public shouldShowRTIResponseHeader(): boolean {

        let rt                  =   this.dateService.rtiFlow;
        let currentState        =   rt.dateOfFirstAppeal !== undefined;

        if (currentState !== this.prevShowRTIResponseState && this.toggleRTIResponseSection == true) {
            this.toggleRTIResponseSection  =   false;
        }

        this.prevShowRTIResponseState = currentState;

        if (this.toggleRTIResponseSection) return true;
        return !currentState;
    }

    public toggleFirstAppealSection: boolean = true;
    public prevShowFirstAppealState: boolean = false;
    public shouldShowFirstAppealHeader() : boolean {

        let rt                  =   this.dateService.rtiFlow;
        let currentState        =    rt.isFAResponseReceived !== undefined;

        if (currentState !== this.prevShowFirstAppealState && this.toggleFirstAppealSection == true) {
            this.toggleFirstAppealSection  =   false;
        }

        this.prevShowFirstAppealState = currentState;

        if (this.toggleFirstAppealSection) return true;
        return !currentState;
    }
}
