import { NgbDateStruct      } 	from 	'@ng-bootstrap/ng-bootstrap';

export interface RTIDateFlowInterface {
    rtiDate                     :   NgbDateStruct;
    rtiReceptionDate            :   NgbDateStruct;
    isLifeAndLiberty            :   boolean;
    isTransferred               :   boolean;
    isAdditionalFeeRequested    :   boolean;
    isInformedAboutThirdParty   :   boolean;

    rtiTransfer                 :   Array<RTITransferDateFlowInterface>;

    feeRequestDate              :   NgbDateStruct;
    feePaymentNotifyDate        :   NgbDateStruct;
    feePaymentNotifyReceiptDate :   NgbDateStruct;

    lastDateForRTIResponse      :   NgbDateStruct;
    isRTIResponseReceived       :   boolean;
    dateOfRTIResponse           :   NgbDateStruct;
    dateOfReceiptOfRTIResponse  :   NgbDateStruct;

    startingDateForFirstAppeal  :   NgbDateStruct;
    lastDateForFirstAppeal      :   NgbDateStruct;
    dateOfFirstAppeal           :   NgbDateStruct;
    dateOfReceiptOfFirstAppeal  :   NgbDateStruct;

    isFAAAdditionalTime         :   boolean;
    lastDateForFAResponse       :   NgbDateStruct;
    isFAResponseReceived        :   boolean;
    dateOfFAResponse            :   NgbDateStruct;
    dateOfReceiptOfFAResponse   :   NgbDateStruct;

    startingDateForSecondAppeal :   NgbDateStruct;
    lastDateForSecondAppeal     :   NgbDateStruct;
};

export interface RTITransferDateFlowInterface {
    startingDateForRTITransfer  :   NgbDateStruct;
    lastDateForRTITransfer      :   NgbDateStruct;
    dateOfRTITransfer           :   NgbDateStruct;
    dateOfReceiptOfRTITransfer  :   NgbDateStruct;
    isTransferredAgain          :   boolean;
    index                       :   Number;
};
