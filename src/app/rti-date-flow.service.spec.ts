import { TestBed, inject } from '@angular/core/testing';

import { RtiDateFlowService } from './rti-date-flow.service';

describe('RtiDateFlowService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [RtiDateFlowService]
        });

        // This is not working. So initializing in each `it`
        inject([RtiDateFlowService], (service: RtiDateFlowService) => {
            service.initialize();
        });
    });

    it('should be created', inject([RtiDateFlowService], (service: RtiDateFlowService) => {
        service.initialize();
        expect(service).toBeTruthy();
    }));

    it('should have rtiFlow object', inject([RtiDateFlowService], (service: RtiDateFlowService) => {
        service.initialize();
        expect(service.rtiFlow).toBeTruthy();
    }));

    it('should have rtiFlow.rtiTranfer, which is an empty array ', inject([RtiDateFlowService], (service: RtiDateFlowService) => {
        service.initialize();
        expect(service.rtiFlow.rtiTransfer).toBeTruthy();
        expect(service.rtiFlow.rtiTransfer.length).toBe(0);
    }));

    it('should have correct `Last Date for RTI` for given `RTI Filing date`', inject([RtiDateFlowService], (service: RtiDateFlowService) => {
        service.initialize();

        let rtiDate             =   {
            day                 :   1,
            month               :   7,
            year                :   2017
        };

        let lastDateForRTI      =   {
            day                 :   31,
            month               :   7,
            year                :   2017
        };

        service.rtiFlow['rtiDate']                      =   rtiDate;
        service.rtiFlow['rtiReceptionDate']             =   rtiDate;

        service.calculateDateFlow();

        expect(service.rtiFlow.lastDateForRTIResponse).toEqual(lastDateForRTI);

        /*
        expect(service.rtiFlow.lastDateForRTIResponse.day).toBe(lastDateForRTI.day);
        expect(service.rtiFlow.lastDateForRTIResponse.month).toBe(lastDateForRTI.month);
        expect(service.rtiFlow.lastDateForRTIResponse.year).toBe(lastDateForRTI.year);
        */
    }));

});
