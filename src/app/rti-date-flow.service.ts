import { Injectable         }   from    "@angular/core";
import { isDevMode          } 	from 	"@angular/core";

import { NgbDateStruct      } 	from 	"@ng-bootstrap/ng-bootstrap";
import * as moment              from    "moment";

import { RTIDateFlowInterface}  from 	"./rti-date-flow.interface";
import { RTITransferDateFlowInterface               }   from 	"./rti-date-flow.interface";

import { Observable, ReplaySubject } from "rxjs/Rx";

declare let ga:any;

@Injectable()
export class RtiDateFlowService {

    public rtiFlow: RTIDateFlowInterface;

    private _resetSubject       =   new ReplaySubject<number>();
    public resetSubject         =   this._resetSubject.asObservable();

    constructor() { }

    initialize() {
        this.rtiFlow            =   <RTIDateFlowInterface>{};
        this.rtiFlow["rtiTransfer"]                     =   [];

        /*
        this.rtiFlow["rtiDate"] =   {
            year                :   2017,
            month               :   7,
            day                 :   14
        };

        this.rtiFlow["rtiReceptionDate"]                =   {
            year                :   2017,
            month               :   7,
            day                 :   16
        };
        this.rtiFlow.isLifeAndLiberty                     =   true;
        //this.rtiFlow.isTransferred                      =   true;
        //this.rtiFlow.isAdditionalFeeRequested             =   true;
        this.rtiFlow.isInformedAboutThirdParty            =   true;
        this.rtiFlow.isRTIResponseReceived          =   false;

        this.rtiFlow["dateOfFirstAppeal"]               =   {
            year                :   2017,
            month               :   7,
            day                 :   14
        };
        this.rtiFlow["dateOfReceiptOfFirstAppeal"]               =   {
            year                :   2017,
            month               :   7,
            day                 :   14
        };
        this.rtiFlow.isFAResponseReceived           =   true;

        this.rtiFlow["dateOfFAResponse"]                =   {
            year                :   2017,
            month               :   7,
            day                 :   20
        };
        this.rtiFlow["dateOfReceiptOfFAResponse"]       =   {
            year                :   2017,
            month               :   7,
            day                 :   20
        };
        */

        this.calculateDateFlow();
    }

    public rtiTransferRadioChange(index: number, selectedVal: boolean) : void {
        //console.log("Change ", index, selectedVal);

        let rt                  =   this.rtiFlow;

        if (selectedVal) {
            if (rt.rtiTransfer.length  === index) {
                let transferObject                  =   {
                    startingDateForRTITransfer      :   null,
                    lastDateForRTITransfer          :   null,
                    dateOfRTITransfer               :   null,
                    dateOfReceiptOfRTITransfer      :   null,
                    isTransferredAgain              :   null,
                    index                           :   rt.rtiTransfer.length + 1
                };
                rt.rtiTransfer.push(transferObject);
            }
        } else if (index < rt.rtiTransfer.length) {
            rt.rtiTransfer.splice(index, rt.rtiTransfer.length);
        }

        // On any change to RTI Transfer selection, reset all sections below it
        rt.feeRequestDate       =   undefined;
        rt.feePaymentNotifyDate =   undefined;
        rt.feePaymentNotifyReceiptDate  =   undefined;
        rt.isRTIResponseReceived    =   undefined;
        rt.dateOfRTIResponse    =   undefined;
        rt.dateOfReceiptOfRTIResponse   =   undefined;
        rt.dateOfFirstAppeal    = undefined;
        rt.dateOfReceiptOfFirstAppeal   =   undefined;
        rt.isFAResponseReceived     =   undefined;
        rt.dateOfFAResponse     =   undefined;
        rt.dateOfReceiptOfFAResponse    =   undefined;
        this._resetSubject.next(0);

        this.calculateDateFlow();
        //console.log(this.rtiFlow);
    }



    public calculateDateFlow() : void {

        let rt                  =   this.rtiFlow;

        let c02_rti_reception;
        let c20_fee_pay_request;
        let c22_payment_reception;

        let c30_last_date_rti_response;
        let c31_responded;
        let c33_rti_response_reception;

        let c40_fa_starting_date;
        let c41_fa_last_date;
        let c43_fa_reception;

        let c50_additional_time;
        let c51_fa_response_last_date;
        let c52_fa_responded;
        let c54_fa_response_reception;

        let c60_sa_starting_date;
        let c61_sa_last_date;

        let baseDate
        let modifier;



        // **************************************************  
        // 1. Reset all Formulae based fields - BEGIN
        // **************************************************  
        rt.lastDateForRTIResponse                       =   null;
        rt.startingDateForFirstAppeal                   =   null;
        rt.lastDateForFirstAppeal                       =   null;
        rt.lastDateForFAResponse                        =   null;
        rt.startingDateForSecondAppeal                  =   null;
        rt.lastDateForSecondAppeal                      =   null;
        for (let i = 0; i < rt.rtiTransfer.length; i++) {
            rt.rtiTransfer[i].startingDateForRTITransfer=   null;
            rt.rtiTransfer[i].lastDateForRTITransfer    =   null;
        }
        // 1. Reset all Formulae based fields - END



        // **************************************************  
        // 2. Transfer Section - BEGIN
        // **************************************************  
        if (!rt.rtiReceptionDate) {
            return;
        }

        rt.isLifeAndLiberty                             =   rt.isLifeAndLiberty;
        c02_rti_reception                               =   this.ngbDateToMomentDate(rt.rtiReceptionDate);
        rt.isAdditionalFeeRequested                     =   rt.isAdditionalFeeRequested;
        rt.isInformedAboutThirdParty                    =   rt.isInformedAboutThirdParty;

        if (rt.isTransferred) {
            for (let i = 0; i < rt.rtiTransfer.length; i++) {

                let t01_start_date;
                let t02_last_date;

                if (i == 0) {
                    t01_start_date= this.ngbDateToMomentDate(rt.rtiReceptionDate);
                } else {
                    if (!rt.rtiTransfer[i-1].dateOfReceiptOfRTITransfer) {
                        return;
                    }
                    t01_start_date= this.ngbDateToMomentDate(rt.rtiTransfer[i-1].dateOfReceiptOfRTITransfer);
                }
                t02_last_date   =   t01_start_date.clone();
                t02_last_date.add(5, "days");

                let rttrans     =   rt.rtiTransfer[i];
                rttrans.startingDateForRTITransfer      =   this.momentDateToNgbDate(t01_start_date);
                rttrans.lastDateForRTITransfer          =   this.momentDateToNgbDate(t02_last_date);

            }

        }

        if (rt.isTransferred === true && !rt.rtiTransfer[0].dateOfReceiptOfRTITransfer) {
            return;
        }
        // 2. Transfer Section - END



        // **************************************************  
        // 3. Additional Fee Section - BEGIN
        // **************************************************  
        if (rt.isAdditionalFeeRequested === undefined) {
            rt.isAdditionalFeeRequested                 =   false;
        }

        if (rt.isAdditionalFeeRequested === true && !rt.feePaymentNotifyReceiptDate) {
            return;
        }

        if (rt.isAdditionalFeeRequested === true && !rt.feeRequestDate) {
            return;
        }
        // 3. Additional Fee Section - END



        // **************************************************  
        // 4. RTI Response - BEGIN
        // **************************************************  
        if (rt.isAdditionalFeeRequested) {
            c20_fee_pay_request =   this.ngbDateToMomentDate(rt.feeRequestDate);
            c22_payment_reception=  this.ngbDateToMomentDate(rt.feePaymentNotifyReceiptDate);
        }

        let lastTransferDate;
        if (rt.isAdditionalFeeRequested) {
            baseDate            =   this.ngbDateToMomentDate(rt.feePaymentNotifyReceiptDate);
            //lastTransferDate    =   c22_payment_reception.clone();

            if (rt.isTransferred) {
                for (let i = rt.rtiTransfer.length - 1; i >= 0; i--) {
                    if (rt.rtiTransfer[i].dateOfReceiptOfRTITransfer) {
                        lastTransferDate=   this.ngbDateToMomentDate(rt.rtiTransfer[i].dateOfReceiptOfRTITransfer);
                        break;
                    }
                }
                if (lastTransferDate == undefined) {
                    lastTransferDate    =   this.ngbDateToMomentDate(rt.rtiReceptionDate);
                }
            } else {
                lastTransferDate        =   this.ngbDateToMomentDate(rt.rtiReceptionDate);
            }

        } else {
            if (rt.isTransferred) {
                for (let i = rt.rtiTransfer.length - 1; i >= 0; i--) {
                    if (rt.rtiTransfer[i].dateOfReceiptOfRTITransfer) {
                        baseDate=   this.ngbDateToMomentDate(rt.rtiTransfer[i].dateOfReceiptOfRTITransfer);
                        break;
                    }
                }
                if (baseDate == undefined) {
                    baseDate    =   this.ngbDateToMomentDate(rt.rtiReceptionDate);
                }
            } else {
                baseDate        =   this.ngbDateToMomentDate(rt.rtiReceptionDate);
            }
            lastTransferDate    =   baseDate.clone();
        }

        if (rt.isLifeAndLiberty === undefined) {
            rt.isLifeAndLiberty            =   false;
        }

        if (rt.isInformedAboutThirdParty === undefined) {
            rt.isInformedAboutThirdParty             =   false;
        }

        if (c20_fee_pay_request === undefined) {
            c20_fee_pay_request =   false;
        }

        if (c22_payment_reception === undefined) {
            c22_payment_reception=  false;
        }
        
        if (rt.isLifeAndLiberty) {
            if (rt.isAdditionalFeeRequested && rt.isInformedAboutThirdParty) {
                modifier        =   40 - (c20_fee_pay_request.clone().diff(lastTransferDate, "days"));
            } else if (rt.isAdditionalFeeRequested == false && rt.isInformedAboutThirdParty) {
                modifier        =   40;
            } else if (rt.isAdditionalFeeRequested && rt.isInformedAboutThirdParty == false) {
                modifier        =   2 - (c20_fee_pay_request.clone().diff(lastTransferDate, "days"));
            } else if (rt.isAdditionalFeeRequested == false && rt.isInformedAboutThirdParty == false) {
                modifier        =   2;
            }
        } else {
            if (rt.isAdditionalFeeRequested && rt.isInformedAboutThirdParty) {
                modifier        =   40 - (c20_fee_pay_request.clone().diff(lastTransferDate, "days"));
            } else if (rt.isAdditionalFeeRequested == false && rt.isInformedAboutThirdParty) {
                modifier        =   40;
            } else if (rt.isAdditionalFeeRequested && rt.isInformedAboutThirdParty == false) {
                modifier        =   30 - (c20_fee_pay_request.diff(lastTransferDate, "days"));
            } else if (rt.isAdditionalFeeRequested == false && rt.isInformedAboutThirdParty == false) {
                modifier        =   30;
            }
        }
        if (modifier < 0) {
            modifier            =   0;
        }

        c30_last_date_rti_response= baseDate.clone().add(modifier, "days");
        rt.lastDateForRTIResponse=  this.momentDateToNgbDate(c30_last_date_rti_response);
        c31_responded           =   rt.isRTIResponseReceived;

        if (c31_responded === undefined) {
            return;
        }

        if (c31_responded && !rt.dateOfReceiptOfRTIResponse) {
            return;
        }
        // 4. RTI Response - END



        // **************************************************  
        // 5. First Appeal - BEGIN
        // **************************************************  
        if (c31_responded) {
            c33_rti_response_reception                  =   this.ngbDateToMomentDate(rt.dateOfReceiptOfRTIResponse);
            let earlierDate;

            if (c33_rti_response_reception.isBefore(c30_last_date_rti_response, 'day')) {
                earlierDate     =   c33_rti_response_reception;
            } else if (c33_rti_response_reception.isSame(c30_last_date_rti_response, 'day')) {
                earlierDate     =   c33_rti_response_reception;
            } else {
                earlierDate     =   c30_last_date_rti_response.clone().add(1, "days");
            }

            //let earlierDate     =   c33_rti_response_reception;
            c40_fa_starting_date=   earlierDate.clone();
            c41_fa_last_date    =   c33_rti_response_reception.clone(); 
            c41_fa_last_date.add(30, "days");
            /*
            if (c33_rti_response_reception.isBefore(c30_last_date_rti_response)) {
                c41_fa_last_date.add(30, "days");
            } else {
                c41_fa_last_date.add(29, "days");
            }
             */
        } else {
            c40_fa_starting_date=   c30_last_date_rti_response.clone();
            c40_fa_starting_date.add(1, "days");
            c41_fa_last_date    =   c40_fa_starting_date.clone(); 
            c41_fa_last_date.add(29, "days");
        }
        rt.startingDateForFirstAppeal                   =   this.momentDateToNgbDate(c40_fa_starting_date);
        rt.lastDateForFirstAppeal                       =   this.momentDateToNgbDate(c41_fa_last_date);
        // 5. First Appeal - END



        // **************************************************  
        // 6. First Appeal Response - BEGIN
        // **************************************************  
        if (!rt.dateOfReceiptOfFirstAppeal) {
            return;
        }

        c43_fa_reception        =   this.ngbDateToMomentDate(rt.dateOfReceiptOfFirstAppeal);

        c50_additional_time     =   rt.isFAAAdditionalTime;
        c51_fa_response_last_date=  c43_fa_reception.clone();
        if (c50_additional_time) {
            c51_fa_response_last_date.add(45, "days");
        } else  {
            c51_fa_response_last_date.add(30, "days");
        }
        rt.lastDateForFAResponse=   this.momentDateToNgbDate(c51_fa_response_last_date);
        // 6. First Appeal Resopnse - END



        // **************************************************  
        // 7. Second Appeal - BEGIN
        // **************************************************  
        c52_fa_responded        =   rt.isFAResponseReceived;

        if (c52_fa_responded == true && !rt.dateOfReceiptOfFAResponse) {
            return;
        }

        if (c52_fa_responded) {
            c54_fa_response_reception                   =  this.ngbDateToMomentDate(rt.dateOfReceiptOfFAResponse);

            let earlierDate;

            if (c54_fa_response_reception.isBefore(c51_fa_response_last_date, 'day')) {
                earlierDate     =   c54_fa_response_reception;
            } else if (c54_fa_response_reception.isSame(c51_fa_response_last_date, 'day')) {
                earlierDate     =   c54_fa_response_reception;
            } else {
                earlierDate     =   c51_fa_response_last_date.clone().add(1, "days");
            }

            c60_sa_starting_date=   earlierDate.clone();
            c61_sa_last_date    =   c54_fa_response_reception.clone();
            c61_sa_last_date.add(90, "days");

            /*
            if (c54_fa_response_reception.isBefore(c51_fa_response_last_date)) {
                c61_sa_last_date.add(90, "days");
            } else {
                c61_sa_last_date.add(89, "days");
            }
             */

        } else {
            c60_sa_starting_date=   c51_fa_response_last_date.clone();
            c60_sa_starting_date.add(1, "days");
            c61_sa_last_date    =   c60_sa_starting_date.clone();
            c61_sa_last_date.add(89, "days");
        }
        rt.startingDateForSecondAppeal                  =   this.momentDateToNgbDate(c60_sa_starting_date);
        rt.lastDateForSecondAppeal                      =   this.momentDateToNgbDate(c61_sa_last_date);
        // 7. Second Appeal - END

    }

    public minDateForFeeRequest() : NgbDateStruct {
        if (this.rtiFlow.isTransferred) {
            let lastTransfer    =   this.rtiFlow.rtiTransfer[this.rtiFlow.rtiTransfer.length - 1];
            return lastTransfer.dateOfReceiptOfRTITransfer;
        } else {
            return this.rtiFlow.rtiReceptionDate;
        }
    }

    public minDateForResponseMadeByPIO() : NgbDateStruct {
        if (this.rtiFlow.isAdditionalFeeRequested) {
            return this.rtiFlow.feePaymentNotifyReceiptDate;
        } else if (this.rtiFlow.isTransferred) {
            let lastTransfer    =   this.rtiFlow.rtiTransfer[this.rtiFlow.rtiTransfer.length - 1];
            return lastTransfer.dateOfReceiptOfRTITransfer;
        } else {
            return this.rtiFlow.rtiReceptionDate;
        }
    }

    public setReceiptDateForC01RTIPetition() : void {
        this.rtiFlow.rtiReceptionDate                   =   this.rtiFlow.rtiDate;
    }

    public setReceiptDateForT04TransferOrder(index: number) : void {
        let transObject         =   this.rtiFlow.rtiTransfer[index];
        transObject.dateOfReceiptOfRTITransfer          =   transObject.dateOfRTITransfer;
    }

    public setReceiptDateForC22FeePayment() : void {
        this.rtiFlow.feePaymentNotifyReceiptDate        =   this.rtiFlow.feePaymentNotifyDate;
    }

    public setReceiptDateForC33RTIResponse() : void {
        this.rtiFlow.dateOfReceiptOfRTIResponse         =   this.rtiFlow.dateOfRTIResponse;
    }

    public setReceiptDateForC43FirstAppeal() : void {
        this.rtiFlow.dateOfReceiptOfFirstAppeal         =   this.rtiFlow.dateOfFirstAppeal;
    }

    public setReceiptDateForC54AppealResponse() : void {
        this.rtiFlow.dateOfReceiptOfFAResponse          =   this.rtiFlow.dateOfFAResponse;
    }

    public ngbDateToMomentDate(dateObject: NgbDateStruct) {
        if (!dateObject) return moment();
        return moment({
            y                   :   dateObject.year,
            M                   :   dateObject.month - 1,
            d                   :   dateObject.day
        });
    }

    public momentDateToNgbDate(dateObject: any):NgbDateStruct {
        return {
            year                :   dateObject.year(),
            month               :   dateObject.month() + 1,
            day                 :   dateObject.date()
        }
    }

    /**
     * Analytics Code
     */

    public toggleFeeRequest() {
        let rt                  =   this.rtiFlow;

        if (rt.isAdditionalFeeRequested) {
            if (!isDevMode()) {
                ga("send", "event", {
                    eventCategory   : 	"custom-event",
                    eventLabel      : 	"ui-action",
                    eventAction     : 	"toggle",
                    eventValue      : 	"fee-request"
                });
            }
        }

        // On any change to RTI Transfer selection, reset all sections below it
        rt.isRTIResponseReceived=   undefined;
        rt.dateOfRTIResponse    =   undefined;
        rt.dateOfReceiptOfRTIResponse   =   undefined;
        rt.dateOfFirstAppeal    = undefined;
        rt.dateOfReceiptOfFirstAppeal   =   undefined;
        rt.isFAResponseReceived =   undefined;
        rt.dateOfFAResponse     =   undefined;
        rt.dateOfReceiptOfFAResponse    =   undefined;
        this._resetSubject.next(1);

        this.calculateDateFlow();
    }

    public toggle3rdParty() {
        if (this.rtiFlow.isInformedAboutThirdParty) {
            if (isDevMode()) return;
            ga("send", "event", {
                eventCategory		: 	"custom-event",
                eventLabel			: 	"ui-action",
                eventAction			: 	"toggle",
                eventValue			: 	"3rd-party"
            });
        }
    }

    public toggleLifeAndLiberty() {
        if (this.rtiFlow.isLifeAndLiberty) {
            if (isDevMode()) return;
            ga("send", "event", {
                eventCategory		: 	"custom-event",
                eventLabel			: 	"ui-action",
                eventAction			: 	"toggle",
                eventValue			: 	"life-liberty"
            });
        }
    }

    public toggle45DaysExtension() {
        if (this.rtiFlow.isFAAAdditionalTime) {
            if (isDevMode()) return;
            ga("send", "event", {
                eventCategory   :   "custom-event",
                eventLabel      :   "ui-action",
                eventAction     :   "toggle",
                eventValue      :   "45-days"
            });
        }
    }

    public toggleResponseFromPIO() {
        let rt                  =   this.rtiFlow;

        if (rt.isRTIResponseReceived) {
            if (!isDevMode()) {
                ga("send", "event", {
                    eventCategory   :   "custom-event",
                    eventLabel      :   "ui-action",
                    eventAction     :   "toggle",
                    eventValue      :   "pio-response"
                });
            }
        } else {
            rt.dateOfRTIResponse                        =   undefined;
            rt.dateOfReceiptOfRTIResponse               =   undefined;
        }

        console.log("toggle Response from PIO");

        // On any change to RTI Transfer selection, reset all sections below it
        rt.dateOfFirstAppeal                            =   undefined;
        rt.dateOfReceiptOfFirstAppeal                   =   undefined;
        rt.isFAResponseReceived                     =   undefined;
        rt.dateOfFAResponse                             =   undefined;
        rt.dateOfReceiptOfFAResponse                    =   undefined;
        this._resetSubject.next(2);

        this.calculateDateFlow();
    }

}
