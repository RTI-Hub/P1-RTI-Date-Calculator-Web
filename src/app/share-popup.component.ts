import { Component          }   from    '@angular/core';

import { NgbActiveModal     }   from    '@ng-bootstrap/ng-bootstrap';

@Component({
    selector                    :   'share-modal',
    templateUrl                 :   './share-popup.component.html',
    styleUrls                   :   ['./share-popup.component.css']
})
export class SharePopupComponent {

    constructor(
        public activeModal      :   NgbActiveModal) {
    }

}
